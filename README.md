# ROS2->ROS1 Bag Conversion Tool

This tool will pull bag data from GCP Storage, convert it to the ROS1 format, and upload the converted bag back to GCP Storage. After the conversion, you can easily pass this data to Kalibr as-is.

## Usage

1. Create virtual environment and install dependencies.
```
python3 -m venv converter
source converter/bin/activate
pip3 install -r requirements.txt
```

2. Download both the intrinsics and extrinsincs ROS2 Bags, convert them, and reupload to GCP Storage (assumes you have `gcloud` CLI installed). Please provide the date and time as formatted in GCP Storage. Example below:
```
sh convert.sh 2022-03-02_16-43-29
```

3. You may now run your Kalibr commands. For example, calibrating the camera via the intrinsic data:
```
rosrun kalibr kalibr_calibrate_cameras \
    --bag bag_files/ \
    --topics /axis/image_raw \
    --models pinhole-equi \
    --target /path/to/target
```
