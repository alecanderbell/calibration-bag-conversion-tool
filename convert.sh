#!/bin/sh

DATE=$(echo $1 | cut -d '_' -f1)
GCP_PATH=gs://smr-stabilization/$DATE/calibration_data
LOCAL_PATH=bag_files

INTRINSICS=intrinsics_$1
EXTRINSICS=extrinsics_$1

GCP_INTRINSICS=$GCP_PATH/$INTRINSICS
GCP_EXTRINSICS=$GCP_PATH/$EXTRINSICS

LOCAL_INTRINSICS=$LOCAL_PATH/$INTRINSICS
LOCAL_EXTRINSICS=$LOCAL_PATH/$EXTRINSICS

#gcloud auth login

echo "Downloading bags from GCP Storage..."

echo $GCP_INTRINSICS
echo $GCP_EXTRINSICS

mkdir $LOCAL_PATH

gcloud config set project "sea-machines-ml1"
gsutil cp -r $GCP_INTRINSICS $LOCAL_PATH &
wait $!
gsutil cp -r $GCP_EXTRINSICS $LOCAL_PATH &
wait $! 

echo "Converting bags from ROS2 to ROS1..."
echo $LOCAL_INTRINSICS
echo $LOCAL_EXTRINSICS

python3 -c "from rosbags.convert import converter; converter.convert_2to1('$LOCAL_INTRINSICS', '${LOCAL_INTRINSICS}_ros1')" &
python3 -c "from rosbags.convert import converter; converter.convert_2to1('$LOCAL_EXTRINSICS', '${LOCAL_EXTRINSICS}_ros1')" &

echo "Uploading converted bags to GCP Storage"
echo ${LOCAL_INTRINSICS}_ros1
echo ${LOCAL_EXTRINSICS}_ros1

gsutil cp -r ${LOCAL_INTRINSICS}_ros1 $GCP_INTRINSICS &
gsutil cp -r ${LOCAL_EXTRINSICS}_ros1 $GCP_EXTRINSICS &

echo "done"

